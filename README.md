# Peer-to-peer Chat

This is a bare bones implementation of peer-to-peer chat using Node.js. Peers
initially connect to the server to get the host address or become host if the
room is empty. The host returns the addresses of the other peers and the chat
history. Peers can then chat without going through the server. If the host 
leaves, the peer with the lowest port number announces its address to the server
to become the host.

To keep the code simple, all connections are opened on `localhost` and port
numbers are used as unique identifiers.

# Installation

```shell
$ npm install
```

## Test

Start the server and a couple of clients. Exchange some chat messages. Close the
host and verify that a new client can join. Close the server and verify that
peers can continue to chat.

```shell
$ node server.js
$ node client.js John
$ node client.js Bill
$ node client.js Tom
```
