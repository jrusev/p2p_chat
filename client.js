const http = require('http');
const WebSocket = require('ws');
const WebSocketServer = require('ws').Server;
const readline = require('readline');
const url = require('url');
const _ = require('underscore');
const SERVER_ADDRESS = 'ws://localhost:8080/';

const ownName = process.argv[2] || '';
const peers = [];
let history = [];
let ownPort;

function getServerConnection() {
  return new WebSocket(`${SERVER_ADDRESS}?port=${ownPort}&name=${ownName}`);
}

// By convention the peer with the lowest port should claim the host.
function maybeClaimHost() {
  const lowestPort = _.min(peers.map(p => p.port));
  if (ownPort < lowestPort) {
    const conn = getServerConnection();
    conn.on('error', err => console.log(`[system] ${err}`));
  }
}

function onPeerClose(peer) {
  console.log(`[system] ${peer.name} left (port ${peer.port})`);
  peers.splice(peers.indexOf(peer), 1);
  maybeClaimHost();
}

function onPeerMessage(peer, msg) {
  const { name, socket } = peer;
  const { type, payload } = JSON.parse(msg);
  if (type === 'GET_HISTORY') {
    socket.send(JSON.stringify({ type: 'HISTORY', payload: { history } }));
  } else if (type === 'HISTORY') {
    history = payload.history;
    console.log(history.join('\n'));
  } else if (type === 'GET_PEERS') {
    const peerData = peers.map(p => _.pick(p, 'name', 'port'));
    socket.send(JSON.stringify({ type: 'PEERS', payload: { peers: peerData } }));
  } else if (type === 'PEERS') {
    payload.peers.forEach(p => connectToPeer(p, false));
  } else if (type === 'CHAT') {
    const chat = `${name}> ${payload.chat}`;
    history.push(chat);
    console.log(chat);
  }
}

// Connect to existing peers.
function connectToPeer(address, peerIsHost) {
  const { name, port } = address;
  if (_.contains([ownPort, ...peers.map(p => p.port)], port)) return;
  const socket = new WebSocket(`ws://localhost:${port}/?port=${ownPort}&name=${ownName}`);
  const peer = { name, port, socket };
  socket.on('open', () => {
    console.log(`[system] Connected to ${name} (port ${port})`);
    peers.push(peer);
    if (peerIsHost) {
      socket.send(JSON.stringify({ type: 'GET_PEERS' }));
      socket.send(JSON.stringify({ type: 'GET_HISTORY' }));
    }
  });

  socket.on('message', msg => onPeerMessage(peer, msg));
  socket.on('close', () => onPeerClose(peer));
}

function getHostFromServer(callback) {
  const conn = getServerConnection();
  conn.on('message', (msg) => {
    const { type, payload } = JSON.parse(msg);
    if (type === 'HOST_ADDRESS') {
      const { name, port } = payload.host;
      callback({ name, port: +port });
      conn.close();
    }
  });
}

// New peers will connect to this server.
const server = http.createServer();
server.listen(() => {
  ownPort = server.address().port;
  console.log(`[system] ${ownName} listening on port ${ownPort}`);
  getHostFromServer(host => connectToPeer(host, true));

  const wss = new WebSocketServer({ server });
  wss.on('connection', (socket) => {
    const { name, port } = url.parse(socket.upgradeReq.url, true).query;
    console.log(`[system] Connected to ${name} (port ${port})`);
    const peer = { name, port: +port, socket };
    peers.push(peer);
    socket.on('message', msg => onPeerMessage(peer, msg));
    socket.on('close', () => onPeerClose(peer));
  });
});

const rl = readline.createInterface(process.stdin, process.stdout);
rl.on('line', (line) => {
  history.push(`${ownName}> ${line}`);
  for (const { socket } of peers) {
    socket.send(JSON.stringify({ type: 'CHAT', payload: { chat: line } }));
  }
});
