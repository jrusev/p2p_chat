const WebSocketServer = require('ws').Server;
const url = require('url');
const PORT = 8080;

let host;

const server = new WebSocketServer({ port: PORT });
server.on('listening', () => console.log(`Server listening on port ${PORT}`));
server.on('connection', (socket) => {
  const { name, port } = url.parse(socket.upgradeReq.url, true).query;

  if (host) {
    socket.send(JSON.stringify({ type: 'HOST_ADDRESS', payload: { host } }));
    socket.close();
  } else {
    console.log(`${name} is now host (port ${port})`);
    host = { name, port };
    socket.on('close', () => (host = null));
  }
});
